/**
 * returns sum of two numbers
 *
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
function sum (a, b, c = 0) {
    const sum = a + b + c;
    return sum;
}
module.exports = sum;
