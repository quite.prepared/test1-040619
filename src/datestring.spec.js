const unixtime = require('./unixtime');
const datestring = require('./datestring');

jest.mock('./unixtime');

unixtime.mockReturnValue(594456600);

describe('datestring test', function() {    
    it ('datestring LTS', function() {
        expect(datestring('LTS')).toBe('6:50:00 AM');
    });
    it ('datestring L', function() {
        expect(datestring('L')).toBe('11/02/1988');
    });
    it ('datestring llll', function() {
        expect(datestring('llll')).toBe('Wed, Nov 2, 1988 6:50 AM');
    });
    it('error "no format given"', function() {
        expect(function() {
            datestring(); 
        }).toThrow('No format given');
    });
    it ('unixtime 594456600', function() {
        expect(unixtime(false, 594456600)).toBe(594456600);
    });
});
