const company = require('./company');

describe('company', function() {
    it('user(3) => data', function() {
        expect(company(3)).toMatchSnapshot();
    });
    it('user(85) => data', function() {
        expect(company(85)).toMatchSnapshot();
    });
    it('letters cant be id', function() {
        expect(function() {
            company('abc');
        }).toThrow('id needs to be integer');
    });  
}); 